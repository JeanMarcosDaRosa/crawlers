from os import walk
import os
import glob
import shutil

fullpath = "./split/"
tam=400
count=0
count_img=0
count_vid=0
idx_folder_img=0
idx_folder_vid=0

def dividir(caminho):
    global count, count_img, count_vid, idx_folder_img, idx_folder_vid, tam, fullpath
    tipo = []
    tipo.append("/*.jpg")
    tipo.append("/*.png")
    tipo.append("/*.avi")
    tipo.append("/*.mp4") 
    for t in tipo:
	for fn in glob.glob(caminho+t):
	    f = glob.os.path.splitext(fn)[0]
	    x = glob.os.path.splitext(fn)[1]
	    arq=f+x
	    if x=='.jpg' or x=='.png':
		if count_img % tam == 0:
		    idx_folder_img+=1
		    if not os.path.isdir(fullpath+"IMG_"+str(idx_folder_img)):
			os.mkdir(fullpath+"IMG_"+str(idx_folder_img))
		fileName = fullpath+"IMG_"+str(idx_folder_img)+arq[len(caminho):]
		if not os.path.exists(fileName):
		    #shutil.copy2(arq,fileName)
		    shutil.move(arq,fileName)
		    count_img+=1
	    if x=='.mp4' or x=='.avi':
		if count_vid % tam == 0:
		    idx_folder_vid+=1
		    if not os.path.isdir(fullpath+"VID_"+str(idx_folder_vid)):
			os.mkdir(fullpath+"VID_"+str(idx_folder_vid))
		fileName = fullpath+"VID_"+str(idx_folder_vid)+arq[len(caminho):]
		if not os.path.exists(fileName):
		    #shutil.copy2(arq,fileName)
		    shutil.move(arq,fileName)
		    count_vid+=1	    
    count = count_img + count_vid

tree = walk('.')
if not os.path.isdir(fullpath):
    os.mkdir(fullpath)
for root, dirs, files in tree:
    if not fullpath[:-1] in root:
	dividir(root)
print count_img,'image files.'
print count_vid,'video files.'
print count,'files done.'