#!/usr/bin/python
#coding: latin-1
# cache_products -> tabela com os links diretos pros produtos, sem paginas intermediarias de links

import sys
import base64
import calendar
import os
import rfc822
import tempfile
import textwrap
import time
import json
import urllib
import urllib2
import urlparse
import thread
import subprocess
import string
from BeautifulSoup import BeautifulSoup, SoupStrainer
import time
import xml.dom.minidom 
import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('CrawlerFB')
ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)
log.addHandler(logHandler) 
log.addHandler(ch)

def getPage(link):
	try:
		pag = urllib2.urlopen(link)
		return BeautifulSoup(pag.read())
	except Exception, e:
		#print 'Erro ao buscar pagina [', e , ']'
		headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
		       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
		       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
		       'Accept-Encoding': 'none',
		       'Accept-Language': 'en-US,en;q=0.8',
		       'Connection': 'keep-alive'}
		request = urllib2.Request(link, '', headers)
		try:
			pag = urllib2.urlopen(request)
			return BeautifulSoup(pag.read())
		except Exception, e:
			#print 'Erro mesmo simulando um navegador: ' ,e
			return None;
 
def parse_url(url_entry, username):
 soup = getPage(url_entry)
 if soup != None:
 	data = json.loads(str(soup))
 	print username+"|"+data['id']

def run_process(access_token): 
 for s in sys.stdin:
 	url_p = 'https://graph.facebook.com/'+str(s).replace('\n', '')+'?access_token='+access_token
 	rt=parse_url(url_p, str(s).replace('\n', ''))
 
 #=================
 return True
 
if len(sys.argv)==2:
	hv=run_process(sys.argv[1])
else:
	print 'USE ', sys.argv[0], ' [access_token]'