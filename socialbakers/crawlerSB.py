#!/usr/bin/python
#coding: latin-1
# cache_products -> tabela com os links diretos pros produtos, sem paginas intermediarias de links

import sys
import base64
import calendar
import os
import rfc822
import tempfile
import textwrap
import time
import urllib
import urllib2
import urlparse
import thread
import subprocess
import string
from BeautifulSoup import BeautifulSoup, SoupStrainer
import time
import xml.dom.minidom 
import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('CrawlerSB')
ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)
log.addHandler(logHandler) 
log.addHandler(ch)

def getPage(link):
  try:
    pag = urllib2.urlopen(link)
    return BeautifulSoup(pag.read())
  except Exception, e:
    #print 'Erro ao buscar pagina [', e , ']'
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    request = urllib2.Request(link, '', headers)
    try:
      pag = urllib2.urlopen(request)
      return BeautifulSoup(pag.read())
    except Exception, e:
      print 'Erro mesmo simulando um navegador: ' ,e
      return None;
 
def parse_url(url_entry):
 collect_u=[]
 soup = getPage(url_entry)
 if soup != False:
  tbls=soup.findAll("table",attrs={'class':'common-table'})
  for tbl in tbls:
   for tbody in tbl:
     if getattr(tbody, 'name', None) == 'tbody':
      for tr in tbody.contents:
       if getattr(tr, 'name', None) == 'tr':
        for td in tr.contents:
         if getattr(td, 'name', None) == 'td':
          for a in td.contents:
           if getattr(a, 'name', None) == 'a':
            if a.has_key('href'):
             lnk=a['href']   
             if lnk.startswith('http://www.socialbakers.com/facebook-pages/'):
              collect_u.append(lnk) 
 return collect_u
def getTextTx(el):
 tx = ''
 if type(el).__name__ == 'Tag':
  tx=el.getText(' ').encode('latin-1', 'ignore')
 if type(el).__name__ == 'NavigableString':
  tx=el.encode('latin-1', 'ignore')  
 return tx

def run_process(): 
 global links
 
 def post_urls(r):
  global contT, nome, usernames
  for lnk in r:
   soup = getPage(lnk)
   if soup != None:
    uls=soup.findAll("ul",attrs={'class':'stats'})
    for ul in uls:
      for li in ul.contents:
       if getattr(li, 'name', None) == 'li':
        for a in li.contents:
         if getattr(a, 'name', None) == 'a':
          if a.has_key('href'):
           if a['href'].startswith('https://www.facebook.com/'):
            str_t = a['href'][25:]
            #str_title = getTextTx(a);
            if str_t.startswith('pages'):
             sp_r = str_t.split('/')
             str_t = sp_r[2]
            #print str_t+'|'+str_title
            usernames.append(str_t)

 
 url_p = 'http://www.socialbakers.com/facebook-pages/'+nome+'/country/brazil/page-'
 paginas = contT
 for i in range(1,paginas+1):
  def job(i,dm):
   global contT
   rt=parse_url(url_p+str(i))
   post_urls(rt)
   contT-=1
  thread.start_new_thread(job,(i,0))
  have=True
 
 while contT>0:
  time.sleep(10)

 #=================
 return True


usernames=[]
nome = ''
contT = 0

if len(sys.argv)==3:
  idx=0
  for arg in sys.argv:
    if idx==1:
      nome = arg
    elif idx==2:
      contT = int(arg)
    idx+=1
  hv=run_process()
  for u in usernames:
    print u
else:
  print 'USE ', sys.argv[0], ' [categoria] [numero de paginas]'