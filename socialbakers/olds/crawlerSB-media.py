#coding: latin-1
# cache_products -> tabela com os links diretos pros produtos, sem paginas intermediarias de links

import sys
import base64
import calendar
import os
import rfc822
import tempfile
import textwrap
import time
import urllib
import urllib2
import urlparse
import thread
import subprocess
import string
from BeautifulSoup import BeautifulSoup, SoupStrainer
import time
import xml.dom.minidom 
import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('CrawlerSB')
ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)
log.addHandler(logHandler) 
log.addHandler(ch)


def getPage(url_entry):
 opener = urllib2.build_opener()
 opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5')]
 try:
  data_Res = opener.open(url_entry, '' ).read()
 except:
  headers = {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5'}
  request = urllib2.Request(url_entry, '', headers)
  try:
   data_Res = urllib2.urlopen(request)
  except:
   print 'Fail page: ', url_entry
   return False
 soup=BeautifulSoup(data_Res) 
 return soup
 
def parse_url(url_entry):
 collect_u=[]
 soup = getPage(url_entry)
 if soup != False:
  tbls=soup.findAll("table",attrs={'class':'common-table'})
  for tbl in tbls:
   for tbody in tbl:
     if getattr(tbody, 'name', None) == 'tbody':
      for tr in tbody.contents:
       if getattr(tr, 'name', None) == 'tr':
        for td in tr.contents:
         if getattr(td, 'name', None) == 'td':
          for a in td.contents:
           if getattr(a, 'name', None) == 'a':
            if a.has_key('href'):
             lnk=a['href']   
             if lnk.startswith('http://www.socialbakers.com/facebook-pages/'):
              collect_u.append(lnk) 
 return collect_u
def getTextTx(el):
 tx = ''
 if type(el).__name__ == 'Tag':
  tx=el.getText(' ').encode('latin-1', 'ignore')
 if type(el).__name__ == 'NavigableString':
  tx=el.encode('latin-1', 'ignore')  
 return tx

def run_process(): 
 global links
 
 def post_urls(r):
  for lnk in r:
   soup = getPage(lnk)
   uls=soup.findAll("ul",attrs={'class':'stats'})
   for ul in uls:
    for li in ul.contents:
     if getattr(li, 'name', None) == 'li':
      for a in li.contents:
       if getattr(a, 'name', None) == 'a':
        if a.has_key('href'):
         if a['href'].startswith('https://www.facebook.com/'):
          str_t = a['href'][25:]
          #str_title = getTextTx(a);
          if str_t.startswith('pages'):
           sp_r = str_t.split('/')
           str_t = sp_r[2]
          #print str_t+'|'+str_title
          print str_t

 url_p = 'http://www.socialbakers.com/facebook-pages/media/country/brazil/page-'
 
 for i in range(1,5):
  def job(i,dm):
   rt=parse_url(url_p+str(i))
   post_urls(rt)
  thread.start_new_thread(job,(i,0))
  have=True
 
 while True:
  time.sleep(10)

 #=================
 return True
 
hv=run_process()
print '-----------Processo Concluido------------'