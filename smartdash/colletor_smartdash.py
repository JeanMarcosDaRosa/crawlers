#coding: latin-1
import sys
import urllib
import urllib2
import bson
import pymongo
import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa import index

poolMongo = pymongo.Connection('mongodb://transend:transend1@162.243.69.60:27017/smartdash')
prod_tbl = poolMongo.smartdash['produtos']

poolCassandra = ConnectionPool('MINDNET', ['localhost:9160'],timeout=100000)
webCache = pycassa.ColumnFamily(poolCassandra, 'web_cache1')

# Lista os produtos da empresa que for passado no parametro
def buscaDadosEmpresa(empresa):
  for prod in prod_tbl.find({'id_empresa':empresa}):
    info_prod = ''
    cat=' categoria defs '
    anuncio = ''
    for x in prod["detalhes_produto"]:
      if x["nome"].lower() == 'categoria':
        cat+=x["valor"]
      if x["nome"].lower() == 'anuncio':
        anuncio=x["valor"]
      else:
        info_prod+='definicao defs '
        info_prod+=(x["nome"]+' = '+x["valor"])
        info_prod+='. \n ' 

    msg_id=prod["link_produto"].encode('hex')
    webCache.insert((msg_id),{'url':prod["link_produto"],'id_empresa':prod["id_empresa"],'pg':(info_prod),'lomadee_url':prod["link_produto"],\
      'termo':'product','usr':'index','purpose':'index','processed':'N','url_icon':'','url_picture':'','id_usr':'',\
      'name_usr':'','story':'','title':prod["desc_produto"],'doc_id':msg_id,'tp':'P','tps':'P','indexed':'N','id':msg_id,'_id':msg_id,'anuncio':anuncio,\
      't_url':'','t_title':prod["desc_produto"],'t_description':'','t_image':prod["img"],"preco":prod["valor_produto"],"prod":prod["link_produto"]})

buscaDadosEmpresa('submarino')