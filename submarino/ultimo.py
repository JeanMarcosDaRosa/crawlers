#submarino.py
import logging
import sys
import time
import thread
from pymongo import MongoClient
from bson.son import SON
import nltk
import os
import os.path
import nlpnet
from apscheduler.schedulers.background import BackgroundScheduler
import semautils as su
from pprint import pprint
from datetime import datetime, timedelta
import json

import urllib2
from BeautifulSoup import BeautifulSoup, SoupStrainer

db = MongoClient().ghost

def parse_metas(soup):
  # parse html itens
  
  descr_P=''.encode('latin-1','ignore')
  descr_All=soup.findAll("h1",attrs={'class':'mp-tit-name'})
  for ds in descr_All:
    for d in ds.contents:
      tx=''
      if type(d).__name__ == 'Tag':
        tx= d.getText(' ').encode('latin-1', 'ignore')
      if type(d).__name__ == 'NavigableString':
        tx=d.encode('latin-1', 'ignore')
      descr_P+=' '+tx

  descr_P=su.s_sanity(descr_P)
  descr_P=descr_P.replace('\t', '').replace('\n', '')
  rt=[]
  rt.append(['descricao',descr_P])

  # preco
  descr_P=''.encode('latin-1','ignore')
  descr_All=soup.findAll("span",attrs={'itemprop':'price'})
  rfir=True
  for ds in descr_All:
    descr_P=''
    for d in ds.contents:
      tx=''
      if type(d).__name__ == 'Tag':
        tx= d.getText(' ').encode('latin-1', 'ignore')
      if type(d).__name__ == 'NavigableString':
        tx=d.encode('latin-1', 'ignore')
      descr_P+=' '+tx
    descr_P=su.s_sanity(descr_P)
    rt.append(['Preço',descr_P])
    rfir=False

  # fotos
  descr_P=''.encode('latin-1','ignore')
  descr_All=soup.findAll("img",attrs={'class':'ip-photo active'})
  rfir=True
  for ds in descr_All:
    descr_P=''
    try:
      if ds.has_key('src'):
        tx=ds['src']
        rt.append(['##imagem',tx])
    except:
      pass
    rfir=False
  # parse categoria
  categ=soup.findAll("span",attrs={'class':'span-bc'})
  caracts_cateh=[]
  cats=[]
  atct=False
  for ittb in  categ:
    lnc=len(ittb.contents)
    for sp in ittb.contents:
      if getattr(sp, 'name', None) == 'span':
        for elmZ in sp.contents:      
          if getattr(elmZ, 'name', None) == 'span':
            for elm in elmZ.contents:
              if getattr(elm, 'name', None) == 'a':
                tx=''
                if type(elm).__name__ == 'Tag':
                  tx=elm.getText(' ').encode('latin-1', 'ignore')
                if type(elm).__name__ == 'NavigableString':
                  tx=elm.encode('latin-1', 'ignore')
                tx=tx.replace(' - ','\n')
                tx=su.decodeUtf8(su.trim(tx))
                caracts_cateh.append(tx)
  # parse caracts table
  rt2=[]
  rt2.append(caracts_cateh)
  matrizcG=[]
  matrizc=[]
  section=soup.findAll("section",attrs={'class':'ficha-tecnica'} )
  for sec in section:
    for tbl_ln in sec:
      if getattr(tbl_ln, 'name', None) == 'table':
        for elm in tbl_ln.contents:
          if getattr(elm, 'name', None) == 'tr':
            tx1=''
            tx2=''
            try:
              ak=elm.contents
            except:
              continue
            for elm2 in elm.contents:
              if getattr(elm2, 'name', None) == 'th':
                for dt1 in elm2.contents:
                  tx=''
                  if type(dt1).__name__ == 'Tag':
                    tx=dt1.getText(' ').encode('latin-1', 'ignore')
                  if type(dt1).__name__ == 'NavigableString':
                    tx=dt1.encode('latin-1', 'ignore')
                  tx=tx.replace(' - ','\n')
                  tx=su.trim(tx)
                  if len(tx)>0:
                    tx = tx.replace('\r', ' ').replace('\n', ' ')
                  tx1+=su.s_sanity(tx)
              if getattr(elm2, 'name', None) == 'td':
                for dt2 in elm2.contents:
                  tx=''
                  if type(dt2).__name__ == 'Tag':
                    tx=dt2.getText(' ').encode('latin-1', 'ignore')
                  if type(dt2).__name__ == 'NavigableString':
                    tx=dt2.encode('latin-1', 'ignore')
                  tx=tx.replace(' - ','\n')
                  tx=su.trim(tx)
                  if len(tx)>0:
                    tx = tx.replace('\r', ' ').replace('\n', ' ')
                  tx2+= su.s_sanity(tx)
            if tx1 != '' and tx2 != '' :
              matrizc.append([tx1,tx2])
        #matrizcG2[0] -> caracts gerais, matrizcG[1] -> especif tecnicas
        matrizcG.append(matrizc)
  rt2.append(rt)
  rt2.append(matrizcG)
  return rt2

matriz_prod=[]

def parse_page(soup,orig_url):
  global start, matriz_prod
  metas=parse_metas(soup)
  cats=[]
  infos=[]
  tab_Caracts=[]
  tab_tecnica=[]
  preco=''
  cats=metas[0]
  infos=metas[1]
  try:
    tab_Caracts=metas[2][0]
  except Exception,e:
    tab_Caracts=[]
    print 'Error:', e, 'url:', orig_url

  # apenas o ultimo item == descricao. os demais serao adicionados na tab diretamente
  t_title=''
  t_image=''
  info_prod={}
  if len(infos) > 0:
    cnd=0
    for i in infos:
      title=su.trim(su.decodeUtf8((i[0]).replace('.','')))
      value=su.trim(su.decodeUtf8(i[1]))
      if title == '##imagem':
        t_image=i[1]
        continue
      if title == 'descricao':
        t_title=value
      else:
        info_prod[title]=value
      cnd+=1
  #==========
  if len(tab_Caracts) > 0 :
    for i in tab_Caracts:
      title=su.trim(su.decodeUtf8((i[0]).replace('.','')))
      value=su.trim(su.decodeUtf8(i[1]))
      if title == 'descricao':
        t_title=value
      else:
        info_prod[title]=value
  #==========
  if len(tab_tecnica) > 0 :
    for i in tab_tecnica:
      title=su.trim(su.decodeUtf8((i[0]).replace('.','')))
      value=su.trim(su.decodeUtf8(i[1]))
      info_prod[title]=value
  #separar ao maximo as definicoes / categorias / preco /
  return {'_id':orig_url, 'categorias': cats,'definicoes':info_prod, 'titulo': t_title, 'imagem':t_image}


def run(dispach, logger, procName, retorno=None):
  try:
    if retorno!=None:
      linksRet = retorno['produtos']
      for pd in linksRet:
        if type(pd) == dict:
          p = db.collect_links_product.find_one({"_id":pd['_id'],"indexed": False, "inproc": False})
          if p!=None:
            link = pd['_id']
            db.collect_links_product.update({"_id": link},{"$set": {"inproc": True}})
            pagina = su.getPage(link)
            if pagina!=None:
              soup = BeautifulSoup(pagina.read().decode('utf-8', 'ignore'))
              prod = parse_page(soup,link)
              db.collect_product.insert(prod)
              print {'produtos': prod['imagem']}
              db.collect_links_product.update({"_id": link},{"$set": {"indexed": True}})
    else:
      links = []
      for l in db.collect_links_product.find({"indexed": False, "inproc": False}):
        links.append(l)
      print {'produtos': links}
  except Exception,e:
    logger.error(e)


run(None, None, "submarino", {'produtos': [{u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/119835275/notebook-samsung-ativ-book-2-intel-core-i3-4gb-500gb-led-14-windows-8.1-branco#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120175761/notebook-dell-inspiron-i14-3442-a10-intel-core-i3-4gb-1tb-led-14-windows-8.1', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120175761/notebook-dell-inspiron-i14-3442-a10-intel-core-i3-4gb-1tb-led-14-windows-8.1#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117742446/notebook-asus-com-intel-core-i3-4gb-500gb-tela-led-14-windows-8', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117742446/notebook-asus-com-intel-core-i3-4gb-500gb-tela-led-14-windows-8#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120434111/notebook-hp-14-r051br-com-intel-core-i3-4gb-500gb-led-14-windows-8.1', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120434111/notebook-hp-14-r051br-com-intel-core-i3-4gb-500gb-led-14-windows-8.1#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120998611/notebook-dell-inspiron-i15-3542-a10-com-intel-core-i3-4gb-1tb-led-15-6-windows-8.1', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120998611/notebook-dell-inspiron-i15-3542-a10-com-intel-core-i3-4gb-1tb-led-15-6-windows-8.1#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120612394/notebook-dell-inspiron-i15-5547-a10-com-intel-core-i5-8gb-2gb-de-memoria-dedicada-1tb-led-hd-15-6-windows-8.1', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120612394/notebook-dell-inspiron-i15-5547-a10-com-intel-core-i5-8gb-2gb-de-memoria-dedicada-1tb-led-hd-15-6-windows-8.1#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/122317989/notebook-hp-14-v061br-intel-core-i5-4gb-1tb-tela-led-14-windows-8.1-prata', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/122317989/notebook-hp-14-v061br-intel-core-i5-4gb-1tb-tela-led-14-windows-8.1-prata#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120452247/notebook-hp-14-r052br-intel-core-i5-4gb-500gb-tela-14-windows-8.1-preto', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120452247/notebook-hp-14-r052br-intel-core-i5-4gb-500gb-tela-14-windows-8.1-preto#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/119835259/notebook-samsung-ativ-book-2-intel-core-i3-4gb-500gb-led-14-windows-8.1-preto', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/119835259/notebook-samsung-ativ-book-2-intel-core-i3-4gb-500gb-led-14-windows-8.1-preto#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/122059300/notebook-samsung-ativ-book-2-intel-core-i5-8gb-2gb-de-memoria-dedicada-1tb-led-15.6-windows-8.1-preto', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/122059300/notebook-samsung-ativ-book-2-intel-core-i5-8gb-2gb-de-memoria-dedicada-1tb-led-15.6-windows-8.1-preto#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/122414931/notebook-ultrafino-dell-vostro-v14t-5470-a50-com-intel-core-i7-8gb-2gb-memoria-dedicada-500gb-led-14-windows-8-perfume-gabriela-sabatini-feminino-eau-de-toilette-60ml-gabriela-sabatini', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/122414931/notebook-ultrafino-dell-vostro-v14t-5470-a50-com-intel-core-i7-8gb-2gb-memoria-dedicada-500gb-led-14-windows-8-perfume-gabriela-sabatini-feminino-eau-de-toilette-60ml-gabriela-sabatini#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117594295/notebook-ultrafino-dell-vostro-v14t-5470-a20-com-intel-core-i5-4gb-2gb-de-memoria-dedicada-500gb-led-14-windows-8', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117594295/notebook-ultrafino-dell-vostro-v14t-5470-a20-com-intel-core-i5-4gb-2gb-de-memoria-dedicada-500gb-led-14-windows-8#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117479861/chromebook-samsung-exynos-5-dual-core-2gb-16gb-led-11-6-sistema-operacional-google-chrome', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117479861/chromebook-samsung-exynos-5-dual-core-2gb-16gb-led-11-6-sistema-operacional-google-chrome#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120612415/notebook-dell-inspiron-i15-5547-a30-com-intel-core-i7-16gb-2gb-de-memoria-dedicada-1tb-led-hd-15-6-touchscreen-windows-8.1', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120612415/notebook-dell-inspiron-i15-5547-a30-com-intel-core-i7-16gb-2gb-de-memoria-dedicada-1tb-led-hd-15-6-touchscreen-windows-8.1#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120612378/notebook-dell-inspiron-i14-5447-a40-com-intel-core-i7-16gb-2gb-de-memoria-dedicada-1tb-led-hd-14-touchscreen-windows-8.1', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/120612378/notebook-dell-inspiron-i14-5447-a40-com-intel-core-i7-16gb-2gb-de-memoria-dedicada-1tb-led-hd-14-touchscreen-windows-8.1#opinioes', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117595394/notebook-ultrafino-dell-vostro-v14t-5470-a60-com-intel-core-i7-8gb-2gb-de-memoria-dedicada-500gb-led-14-touchscreen-windows-8', u'root': u'submarino', u'inproc': False}, {u'indexed': False, u'_id': u'http://www.submarino.com.br/produto/117595394/notebook-ultrafino-dell-vostro-v14t-5470-a60-com-intel-core-i7-8gb-2gb-de-memoria-dedicada-500gb-led-14-touchscreen-windows-8#opinioes', u'root': u'submarino', u'inproc': False}]})