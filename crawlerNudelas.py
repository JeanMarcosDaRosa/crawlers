#!/usr/bin/python
#coding: latin-1

import urllib2
import sys
import os
import thread
import time
from BeautifulSoup import BeautifulSoup, SoupStrainer
import string
import threading

link = 'http://www.nudelas.com/'
path = '/.git/galeria/nudelas/'
quebrados=[]
qtdPosts=0
qtdImgs=0
contThreads = 0
contTotImagens=0
def getPage(urlPagina):
  try:
    pag = urllib2.urlopen(urlPagina)
    return BeautifulSoup(pag.read())
  except Exception, e:
    headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
           'Accept-Encoding': 'none',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive'}
    request = urllib2.Request(urlPagina, '', headers)
    try:
      pag = urllib2.urlopen(request)
      return BeautifulSoup(pag.read())
    except Exception, e:
      print 'Erro simulando um navegador: ' ,e
      return None;

def processPageArt(linkPageArt, dm):
  global link, path, qtdPosts, qtdImgs, contThreads, quebrados
  soup = getPage(linkPageArt)
  contImgs=0
  nomePasta = 'outras'
  nomeArq=0
  #Pega a data da postagem para colocar como nome da pasta
  tagsDataPubl = soup.findAll('meta', attrs={'property':'article:published_time'})
  for d in tagsDataPubl:
    if getattr(d, 'name', None) == 'meta':
      if(d.has_key('content')):
	dataPubl = d['content']
	if len(dataPubl)>10:
	  nomePasta=path+dataPubl[:10]+'/'
  
  h2 = soup.findAll('h2', attrs={'class': 'titulopost'})
  for n in h2:
    for a in n:
      if getattr(a, 'name', None) == 'a':
	if(a.has_key('href')):
	  nomeArq=a['href'][len(link):-1]
  
  tagsIMG = soup.findAll('img')
  for m in tagsIMG:
    if getattr(m, 'name', None) == 'img':
      if(m.has_key('src')):
	linkIMG = m['src']
	if 'www.nudelas.com/posts/' in linkIMG:
	  if 'wp.com/' in linkIMG:
	    linkIMG = 'http://'+linkIMG[linkIMG.find('wp.com/')+len('wp.com/'):]
	  if not os.path.exists(nomePasta):
	    try:
	      os.mkdir(nomePasta)
	    except:
	      pass
	  nImg = linkIMG.split('/')
	  defPath = str(nomePasta)+str(nomeArq)+'-'+str(contImgs)+(os.path.splitext(nImg[-1])[1])
	  if not os.path.isfile(defPath):
	    try:
	      img = urllib2.urlopen(linkIMG)
	      output = open(defPath,'wb')
	      output.write(img.read())
	      output.close()
	      contImgs+=1
	    except:
	      quebrados.append(linkIMG)
  qtdPosts+=1
  qtdImgs+=contImgs
  contThreads -= 1

def getLinksArt(pagina):
  global contThreads
  soup = getPage(pagina)
  tagsA = soup.findAll('a', attrs={'class':'more-link'})
  for a in tagsA:
    if a.has_key('href'):
      thread.start_new_thread(processPageArt,(a['href'],0))
      #processPageArt(a['href'],0)
      contThreads+=1
  while contThreads>0:
    time.sleep(1)
	
pag = getPage(link)
tagsA = pag.findAll('a', attrs={'class':'page-numbers'})
total=0
for a in tagsA:
  x=int(a.getText().replace('.', ''))
  if x>total:
    total=x

pgIni=1
pgFim=int(total)
if len(sys.argv)==2:
  pgIni = int(sys.argv[1])
elif len(sys.argv)==3:
  pgIni = int(sys.argv[1])
  pgFim = int(sys.argv[2])
elif len(sys.argv)==4:
  pgIni = int(sys.argv[1])
  pgFim = int(sys.argv[2])
  path = sys.argv[3]
  if path[-1:]!='/':
    path=path+'/'

print path

if not os.path.exists(path):
  os.mkdir(path)
  
print 'Captura da pagina ['+str(pgIni)+'] ate ['+str(pgFim)+'] - do total de '+str(total)+' paginas'

for i in range(pgIni, pgFim+1):
  print 'Pagina numero [', i, ']'
  getLinksArt('http://www.nudelas.com/page/'+str(i)+'/')
  print 'Total de Posts:', qtdPosts, ', Novas imagens:', qtdImgs
  print '-----------------------------------------------'
  contTotImagens+=qtdImgs
  qtdImgs=0
  qtdPosts=0
print 'Capturou no total ['+str(contTotImagens)+'] imagens'
if len(quebrados) > 0:
  f=open('quebrados_nudelas.txt', 'a')
  for q in quebrados:
    f.write(q.encode('latin-1')+'\n')
  f.close()