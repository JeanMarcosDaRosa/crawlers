#!/usr/bin/python
#coding: latin-1

from os import walk
import os
import sys
import glob
# Modulo principal do PIL
from PIL import Image
# Modulo de filtros
from PIL import ImageFilter

count = 0
qtdFiles=0
filesOnPath=0
pathFiles='.'
pathMin=pathFiles+'./miniaturas'
if len(sys.argv)==2:
    pathFiles = sys.argv[1]
    if pathFiles[-1:]=='/':
	pathFiles=pathFiles[:-1]
	if not os.path.isdir(pathFiles):
	    print 'Diretorio[',pathFiles,'] nao existe!'
	    sys.exit(0)
    name=pathFiles.split("/")[-1]
    raiz=""
    dirs=pathFiles[1:].split("/")
    if pathFiles[0] == '/':
	raiz="/"
	dirs=pathFiles[1:].split("/")
    l=len(dirs)
    cn=1
    for n in dirs:
	if cn < l:
	    raiz+=n+"/"
	    cn+=1
    pathMin=raiz+'miniaturas/'+name
    if not os.path.isdir(raiz+'miniaturas'):
	os.mkdir(raiz+'miniaturas')
    if not os.path.isdir(pathMin):
	os.mkdir(pathMin)
	if not os.path.isdir(pathMin):
	    print 'Diretorio[',pathMin,'] nao existe!'
	    sys.exit(0)  
elif len(sys.argv)==3:
    pathFiles = sys.argv[1]
    pathMin = sys.argv[2]
    if pathFiles[-1:]=='/':
	pathFiles=pathFiles[:-1]
	if not os.path.isdir(pathFiles):
	    print 'Diretorio[',pathFiles,'] nao existe!'
	    sys.exit(0)
    if pathMin[-1:]=='/':
	pathMin=pathMin[:-1]
    if not os.path.isdir(pathMin):
	print 'Diretorio[',pathMin,'] nao existe!'
	sys.exit(0)    
	
def qtdMiniatura(caminho):
    countToP=0
    tipo = []
    tipo.append("/*.jpg")
    tipo.append("/*.png")
    tipo.append("/*.jpeg")
    for t in tipo:
	for fn in glob.glob(caminho+t):
	    countToP+=1
    return countToP

def percentage(percent, whole):
    return myformat((float(percent) / float(whole)) * 100.0)

def myformat(x):
    return ('%.2f' % x).rstrip('0').rstrip('.')

def miniatura(caminho):
    global count, pathMin, pathFiles, qtdFiles, filesOnPath
    carac='/'
    ff=False
    tipo = []
    tipo.append("/*.jpg")
    tipo.append("/*.png")
    tipo.append("/*.jpeg")
    for t in tipo:
	for fn in glob.glob(caminho+t):
	    # Retorna o nome do arquivo sem extensao
	    f = glob.os.path.splitext(fn)[0]
	    x = glob.os.path.splitext(fn)[1]
	    arq=f+x
	    filesOnPath+=1
	    if not os.path.isfile(pathMin+arq[len(pathFiles):]):
		try:
		    imagem = Image.open(fn)
		    width, height = imagem.size
		    # Se maior que 1 MB entra e faz uma miniatura
		    if width > 400 or height > 300:
			# Cria thumbnail (miniatura) da imagem
			# de tamanho 100x100 usando antialiasing
			#print 'Processando:', fn
			#print 'Tamanho: ', os.stat(fn).st_size, 'bytes'
			imagem.thumbnail((400,300), Image.ANTIALIAS)
			# Filtro suaviza a imagem
			imagem = imagem.filter(ImageFilter.SMOOTH)
			# verifica se o diretorio existe, caso nao exista cria
			out=pathMin+caminho[len(pathFiles):]
			if not os.path.isdir(out):
			    os.mkdir(out)
			imagem.save(pathMin+arq[len(pathFiles):])
			count+=1
		except:
		    continue
	    '''   | / - \ | / - \ |   '''
	    if carac=='|':
		carac='/'		    
	    elif carac=='/':
		carac='-'
	    elif carac=='-':
		carac='\\'
	    elif carac=='\\':
		carac='|'	    
	    print '\rGerando' , carac, percentage(filesOnPath, qtdFiles), '%   ',
	    sys.stdout.flush()
    
print 'Salvando miniaturas em:',pathMin
tree = walk(pathFiles)

treeC = walk(pathFiles)
for root, dirs, files in treeC:
    qtdFiles+=qtdMiniatura(root)
    
for root, dirs, files in tree:
    miniatura(root)
    
print		
print '------------------'
print count, 'images done.'