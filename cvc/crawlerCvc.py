#coding: latin-1
# cache_products -> tabela com os links diretos pros produtos, sem paginas intermediarias de links

import sys
sys.path.append("/home/jean/Programacao/Python/componentes/pycassa")
sys.path.append("/home/jean/Programacao/Python/componentes")

import base64
import calendar
import os
import rfc822
import tempfile
import textwrap
import time
import urllib
import urllib2
import urlparse
import thread
import umisc 
 
import subprocess
import string

from BeautifulSoup import BeautifulSoup, SoupStrainer
 
import time

import xml.dom.minidom 

import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa import index


import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('CrawlerCvc')


ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)

log.addHandler(logHandler) 
log.addHandler(ch) 


pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10)
tab2 = pycassa.ColumnFamily(pool2, 'cache_products')
tab2.truncate();
 
def parse_url(url_entry):
 
 def s_sanity(st):
   r=''
   ant=''
   for i in st:
    if i == ' ' and ant == ' ':
     continue
    else:
      r+=i
    ant=i  
   return r   
 
 def limpar(tx):
  tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\n':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\r':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\r':
   tx=tx[:-1]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\n':
   tx=tx[:-1]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\n':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\r':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\r':
   tx=tx[:-1]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\n':
   tx=tx[:-1]
   tx=umisc.trim(tx)  
  return tx
 
 collect_u=[]
 opener = urllib2.build_opener()
 opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5')]
 try:
  data_Res = opener.open(url_entry, '' ).read()
 except:
  headers = {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5'}
  request = urllib2.Request(url_entry, '', headers)
  data_Res = urllib2.urlopen(request)
  #data_Res = urllib2.Request(url_entry.encode('utf-8'))
 #======================
     
 soup=BeautifulSoup(data_Res)
 descr_P=''.encode('latin-1','ignore')
 divs=soup.findAll("div",attrs={'class':'packageContainer'} )
 for div in  divs:
  for tbl in div.contents: 
   if getattr(tbl, 'name', None) == 'table':
     #for ittb in  tbl:
     for elm in tbl.contents:
      if getattr(elm, 'name', None) == 'tbody':
       for tr in elm :
        if getattr(tr, 'name', None) == 'tr':
         preco=''
         lnk=''
         nome=''
         try:
          ak=tr.contents     
         except:
          continue
         for td in tr.contents:
            if getattr(td, 'name', None) == 'td':
             if td.attrs[0][1] == 'preco':
              preco='A partir de'
              for el in td.contents:
               tx=''
               if type(el).__name__ == 'Tag':
                tx=el.getText(' ').encode('latin-1', 'ignore')
               if type(el).__name__ == 'NavigableString':
                tx=el.encode('latin-1', 'ignore')
               preco+=' '+limpar(tx)
             elif td.attrs[0][1] == 'comprar':
              lnk=''
              for a in td.contents:
               if getattr(a, 'name', None) == 'a':
                if a.has_key('href'):
                 lnk=a['href']    
             elif td.attrs[0][1] == 'nome':
              nome=''
              for n in td.contents:
               if type(n).__name__ == 'Tag':
                tx=n.getText(' ').encode('latin-1', 'ignore')
               if type(n).__name__ == 'NavigableString':
                tx=n.encode('latin-1', 'ignore')
               nome+=' '+limpar(tx)
                  
         preco=s_sanity(limpar(preco.replace('<span class="parcelas">', '').replace('</span>', '')))
         collect_u.append(['http://www.cvc.com.br/'+lnk, limpar(nome), limpar(preco)])
 print 'url(',len(collect_u),'):',url_entry           
 return collect_u
 
def run_process(): 
 
 def post_urls(r):
   for lnk, nome, preco in r:
    try:
      # verifica se tem no banco de dados
      r=tab2.get(lnk)
    except:
     # se nao tiver, insere
     print 'insert into product.table:',lnk
     try:
       tab2.insert(lnk,{"nome":nome,"preco":preco,"INDEXED":'N'})
     except :
       log.exception("ERROR")         
       
 print 'init cache links...'

 url_p = 'http://www.cvc.com.br/todos-pacotes-cvc.aspx?searchType=National'
 
 print 'run.url:',url_p
 rt=parse_url(url_p)
 post_urls(rt)
 have=True 
 
 url_p = 'http://www.cvc.com.br/todos-pacotes-cvc.aspx?searchType=International'
 
 print 'run.url:',url_p
 rt=parse_url(url_p)
 post_urls(rt)
 have=True 

 return True
 
hv=run_process()
print 'Processo Concluido' 