#coding: latin-1
# cache_products -> tabela com os links diretos pros produtos, sem paginas intermediarias de links

import sys
sys.path.append("/home/jean/Programacao/Python/componentes/pycassa")
sys.path.append("/home/jean/Programacao/Python/componentes")
import base64
import calendar
import os
import rfc822
import tempfile
import textwrap
import time
import urllib
import urllib2
import urlparse
import thread
import umisc  
import subprocess
import string
from BeautifulSoup import BeautifulSoup, SoupStrainer
import time
import xml.dom.minidom 
import pycassa
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily
from pycassa import index
import logging
from StringIO import StringIO

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger('CrawlerHotelUrbano')
ch  = logging.StreamHandler ()
lbuffer = StringIO ()
logHandler = logging.StreamHandler(lbuffer)
log.addHandler(logHandler) 
log.addHandler(ch) 

pool2 = ConnectionPool('MINDNET', ['localhost:9160'],timeout=10)
tab2 = pycassa.ColumnFamily(pool2, 'cache_products')
tab2.truncate();
 
def parse_url(url_entry):

 def s_sanity(st):
   r=''
   ant=''
   for i in st:
    if i == ' ' and ant == ' ':
     continue
    else:
      r+=i
    ant=i  
   return r   
 
 def limpar(tx):
  tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\n':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\r':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\r':
   tx=tx[:-1]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\n':
   tx=tx[:-1]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\n':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[0] == '\r':
   tx=tx[1:]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\r':
   tx=tx[:-1]
   tx=umisc.trim(tx)
  if len(tx)>0 and tx[-1] == '\n':
   tx=tx[:-1]
   tx=umisc.trim(tx)  
  return tx
 
 collect_u=[]
 opener = urllib2.build_opener()
 opener.addheaders = [('User-agent', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5')]
 try:
  data_Res = opener.open(url_entry, '' ).read()
 except:
  headers = {'User-Agent' : 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5'}
  request = urllib2.Request(url_entry, '', headers)
  data_Res = urllib2.urlopen(request)
  #data_Res = urllib2.Request(url_entry.encode('utf-8'))
 #======================

 def getTextTx(el):
  tx = ''
  if type(el).__name__ == 'Tag':
   tx=el.getText(' ').encode('latin-1', 'ignore')
  if type(el).__name__ == 'NavigableString':
   tx=el.encode('latin-1', 'ignore')  
  return tx
 
 
 soup=BeautifulSoup(data_Res)
 descr_P=''.encode('latin-1','ignore')
 divs=soup.findAll("div",id="leftbar")
 preco=''
 img=''
 nome=''
 for div in divs:
  for elm in div:
   if getattr(elm, 'name', None) == 'a':
    if elm.has_key('href'):
     lnk=elm['href']      
    for a in elm.contents: 
     if getattr(a, 'name', None) == 'div':
      for d in a.contents:
       if getattr(d, 'name', None) == 'div':
        if d.attrs[0][1] == 'tarjaDesconto txtCenter' or d.attrs[0][1] == 'info-juros txtCenter':
         for p in d.contents:
          preco+= ' '+getTextTx(p)
        elif d.attrs[0][1] == 'box-info-oferta': 
         for infobox in d.contents:
          if getattr(infobox, 'name', None) == 'div':
           if infobox.attrs[0][1] == 'info-hotel aLeft bdRightDashed':
            for dado in infobox:
             if getattr(dado, 'name', None) == 'h2':
              if dado.attrs[0][1] == 'marinho font20':
               nome= getTextTx(dado)
             elif getattr(dado, 'name', None) == 'p':
              if dado.attrs[0][1] == 'darkGray font12':
               nome+= ', '+getTextTx(dado)
           elif infobox.attrs[0][1] == 'verOferta aRight':
            preco+= ', '
            for p in infobox.contents:
             preco+= ' '+getTextTx(p)
       elif getattr(d, 'name', None) == 'img':
        if d.has_key('data-src'):       
         img = d['data-src']
                   
    preco=s_sanity(limpar(preco))
    collect_u.append([lnk, limpar(nome), limpar(preco), img])
 print 'url(',len(collect_u),'):',url_entry           
 return collect_u

def run_process(): 
 
 def post_urls(r):
   for lnk, nome, preco, img in r:
    try:
      # verifica se tem no banco de dados
      r=tab2.get(lnk)
    except:
     # se nao tiver, insere
     print 'insert into product.table:',lnk
     try:
       tab2.insert(lnk,{"nome":nome,"preco":preco, "imagem":img,"INDEXED":'N'})
     except :
       log.exception("ERROR")         
       
 print 'init cache links...'

 url_p = 'http://www.hotelurbano.com/?pag='
 for i in range(1,358):
   print 'run.url:',url_p+str(i)
   rt=parse_url(url_p+str(i))
   post_urls(rt)
   have=True
   
 return True
 
hv=run_process()
print 'Processo Concluido' 