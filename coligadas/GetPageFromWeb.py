from bs4 import BeautifulSoup
import urllib3
import requests
# Pega carrega pagina da web e retorna um BeautifulSoup da pagina
class Page:

    def __init__(self, link):
        self.link = link

    def getPage(self):
        try:
            http = urllib3.PoolManager(timeout=30)
            pag = http.request('GET', self.link, retries=3)

            # Método com urllib3
            pag = pag.read()
            if pag != None and len(pag) > 0:
                return BeautifulSoup(pag, "html.parser")

            # Método com requests
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36'
            }
            pag = requests.get(self.link, headers=headers)
            pag = pag.content
            if pag != None and len(pag) > 0:
                return BeautifulSoup(pag, "html.parser")

            return None
        except Exception as e:
            print (e)
            return BeautifulSoup('<html><head><head/></html>', "html.parser")

    def getLink(self):
        try:
            links = []
            for l in Page(self.link).getPage().find_all('a'):
                if l.has_attr('href'):
                    links.append(l['href'])

            return set(links)
        except Exception as e:
            print ('GetPageFromWeb ', e)


if __name__ == '__main__':
    link = 'http://www.coligadas.com.br'
    print (Page(link).getLink())
