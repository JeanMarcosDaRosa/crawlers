import GetPageFromWeb
from termcolor import colored
from sqlalchemy import *

class Crawler:

    def __init__(self, link):
        self.link = link
        self.msgNewLink = colored('NewLink: [{:s}]', 'blue')
        self.msgNewLinkPossibleImovel = colored('Possivel imovel [{:s}]', 'yellow')
        self.msgOldLink = colored('GetLinkFromPage: [{:s}]', 'red')
        #conexao com o banco
        self.engine = create_engine('postgresql://postgres:postgres@localhost/crawlers')
        self.session = self.engine.connect()
        self.metadata = MetaData(self.engine)
        self.table = Table('coligadas_crawler', self.metadata, autoload=True)
        #metadata.bind.echo = True

    def select(self, status=0):
        ins = self.table.select().where(self.table.c.status==status)
        ins.compile().params
        return self.session.execute(ins).fetchall()

    def select_exist(self, link=None):
        ins = self.table.select().where(self.table.c.link==link)
        ins.compile().params
        lst = self.session.execute(ins).fetchall()
        return (lst != None and len(lst)>0)

    def insert(self, **values):
        if not self.select_exist(values['link']):
            ins = self.table.insert().values(values)
            ins.compile().params
            self.session.execute(ins)

    def update(self, link=None):
        try:
            ins = self.table.update().where(self.table.c.link==link).values(status=1)
            ins.compile().params
            self.session.execute(ins)
        except:
            pass


    # Para links, minha escolha foi de remover caracter que não pertençam a tabela ascii
    def convertToAscii(self, content):
        return content.encode('ascii', 'ignore').decode('ascii')

    def validationLink(self, link):
        if not 'javascript' in link and not link == '#' and not 'realmedia' in link and link != '':
            if not link.startswith('http://') and not link.startswith('https://'):
                return self.convertToAscii(link)
        else:
            return None

    # Com base nesse método será possivel criar filtros de links para informar
    # se ele tem caracteristicas de ser de um imovel.
    def checksPossibleImovel(self, link):
        if '/imovel/' in link and not '#opinioes' in link:
            return True

    def startCollection(self):
        for l in GetPageFromWeb.Page(self.link).getLink():
            l = self.validationLink(l)
            if l != None:
                l = 'http://www.coligadas.com.br/'+l
                if self.checksPossibleImovel(l) == True:
                    self.insert(link=l, status=0, possivel_imovel=1)
                    print(self.msgNewLinkPossibleImovel.format(l))
                else:
                    self.insert(link=l, status=0, possivel_imovel=0)
                    print(self.msgNewLink.format(l))
        self.update(link=self.link)

        try:
            while True:
                links = self.select(status=0)
                if links:
                    print(len(links))
                    for l in links:
                        print(self.msgOldLink.format(l.link))
                        newLinks = GetPageFromWeb.Page(l.link).getLink()
                        for nl in newLinks:
                            nl = self.validationLink(nl)
                            if nl != None:
                                nl = 'http://www.coligadas.com.br/'+nl
                                if self.checksPossibleImovel(nl) == True:
                                    self.insert(link=nl, status=0, possivel_imovel=1)
                                    print(self.msgNewLinkPossibleImovel.format(nl))
                                else:
                                    self.insert(link=nl, status=0, possivel_imovel=0)
                                    print(self.msgNewLink.format(nl))
                        self.update(link=l.link)

        except Exception as e:
            self.update(link=l.link)
            print('CrawlerColigadas', e)


if __name__ == '__main__':
    Crawler('http://www.coligadas.com.br').startCollection()
