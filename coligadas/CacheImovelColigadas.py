from prettytable import PrettyTable
from termcolor import colored
import GetPageFromWeb
from sqlalchemy import *
import datetime


class Cache:

    def __init__(self):
        #conexao com o banco
        self.engine = create_engine('postgresql://postgres:postgres@localhost/crawlers')
        self.session = self.engine.connect()
        self.metadata = MetaData(self.engine)
        self.table_cache = Table('coligadas_cache', self.metadata, autoload=True)
        self.table_crawler = Table('coligadas_crawler', self.metadata, autoload=True)


    def select_news(self, possivel_imovel=1, seconds=0):
        time_last = datetime.datetime.now()+ datetime.timedelta(seconds=-seconds)
        ins = self.table_crawler.select().where(
            and_(
                self.table_crawler.c.possivel_imovel==possivel_imovel,
                or_(
                    self.table_crawler.c.ultima_atualizacao==None,
                    self.table_crawler.c.ultima_atualizacao<=time_last,
                )
            )

        ).limit(10)
        ins.compile().params
        return self.session.execute(ins).fetchall()


    def select_exist(self, link=None):
        ins = self.table_cache.select().where(self.table_cache.c.link==link)
        ins.compile().params
        lst = self.session.execute(ins).fetchall()
        return (lst != None and len(lst)>0)


    def insert_cache(self, **values):
        try:
            if not self.select_exist(values['link']):
                ins = self.table_cache.insert().values(values)
                ins.compile().params
                self.session.execute(ins)
            else:
                self.update_cache(**values)
        except Exception as e:
            print(e)


    def update_cache(self, **values):
        try:
            ins = self.table_cache.update().where(self.table_cache.c.link==values['link']).values(
                preco=values['preco'], descricao=values['descricao'],
                nome=values['nome'], categoria=values['categoria'], imagem=values['imagem']
            )
            ins.compile().params
            self.session.execute(ins)
        except:
            pass


    def update_crawler(self, link=None):
        try:
            ins = self.table_crawler.update().where(self.table_crawler.c.link==link).values(
                ultima_atualizacao=func.now()
            )
            ins.compile().params
            self.session.execute(ins)
        except:
            pass


    # Remove todos os caracteres que não pertencem a tabela de ascii Latin-1.
    def convertToLatin(self, content):
        return content.encode('latin-1', 'ignore').decode('latin-1')


    def getImg(self, pag):
        x = pag.find("div", attrs={"class": "imgInicialImovel"})
        s = x["style"]
        return s[s.find("('")+2:s.find("')")]


    def getName(self, pag):
        x = pag.find("div", attrs={"class": "tituloPgImovel"})
        return self.convertToLatin(x['title'])


    def getPrice(self, pag):
        divAttrs = pag.find("div", attrs={"class": "infoImovelPg"})
        divFields = divAttrs.findAll("div", attrs={"class": "campo"})
        for d in divFields:
            info = d.find("div", attrs={"class": "titulo"})
            if info!=None and 'valor' in info.text.lower():
                valor = d.find("div", attrs={"class": "info"})
                if valor!=None and valor.text!=None and len(valor.text)>3:
                    return float(valor.text[3:].replace('.', '').replace(',', '.'))
        return None


    def getCategory(self, pag):
        categorys = []
        category = pag.find('div', attrs={"class": "barraPagina"})
        if category!=None:
            for x in category.strings:
                categorys.append(self.convertToLatin(x.strip()))
        return categorys[:-1]


    def getDescription(self, pag):
        tableAll = pag.find('div', attrs={"class": "caracteristicas"})
        description = ""
        for t in tableAll.strings:
            if 'function' not in t:
                description += '\n'+ self.convertToLatin(t)
            else:
                break
        return description


    def getPage(self):
        while True:
            try:
                table = PrettyTable([colored('Link', 'blue'), colored('Nome', 'blue'), colored('Valor', 'red'), 'Operação', 'Categoria', 'Sub-categoria', 'Resultado'])
                dados = self.select_news(possivel_imovel=1, seconds=3600)
                if len(dados) == 0:
                    print('End of link :)')
                    break
                for link in dados:
                    try:
                        pag = GetPageFromWeb.Page(link.link).getPage()
                        name = self.getName(pag)
                        price = self.getPrice(pag)
                        category = self.getCategory(pag)
                        description = self.getDescription(pag)
                        picture = self.getImg(pag)
                        self.insert_cache(preco=price, descricao=description, nome=name, categoria=','.join(category), imagem=picture, link=link.link)
                        self.update_crawler(link.link)
                        table.add_row([link.link.replace('http://www.coligadas.com.br/', ''), name, price, category[0] if len(category) > 0 else '', category[1] if len(category) > 1 else '', category[2] if len(category) > 2 else '', 'OK'])
                    except Exception as e:
                        print('FOR', e)
                        table.add_row([link.link.replace('http://www.coligadas.com.br/', ''), '', '', '', '', '', 'FAIL'])
                        self.update_crawler(link.link)

                print(table)

            except Exception as e:
                print('WHILE', e)


if __name__ == '__main__':
    Cache().getPage()
