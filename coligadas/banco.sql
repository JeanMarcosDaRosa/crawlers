-- Database name: crawlers

-- Table: coligadas_cache

-- DROP TABLE coligadas_cache;

CREATE TABLE coligadas_cache
(
  link character varying(512) NOT NULL,
  descricao character varying(2048),
  nome character varying(256),
  categoria character varying(256),
  imagem character varying(512),
  preco double precision,
  CONSTRAINT pk_coligadas_cache PRIMARY KEY (link)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coligadas_cache
  OWNER TO postgres;


-- Table: coligadas_crawler

-- DROP TABLE coligadas_crawler;

CREATE TABLE coligadas_crawler
(
  link character varying(512) NOT NULL,
  status integer,
  possivel_imovel integer,
  ultima_atualizacao timestamp without time zone,
  CONSTRAINT pk_coligadas_crawler PRIMARY KEY (link)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE coligadas_crawler
  OWNER TO postgres;
